package io.nise.rover;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

public class PlanetTests {

    Planet planet = new Planet(8);

    @Test
    void shouldHaveSize() {
        assertThat(planet.getRadius()).isEqualTo(8);
    }

    @ParameterizedTest
    @ValueSource(ints = { 4, 12, 20, 28 })
    void shouldWrapXAlongTheLongitude(int xPosition) {
        assertThat(planet.wrap(new Position(xPosition, 0))).usingRecursiveComparison().isEqualTo(new Position(4, 0));
    }

    @Test
    void shouldWrapY() {
        assertThat(planet.wrap(new Position(0, 7))).usingRecursiveComparison().isEqualTo(new Position(0, 7));
        assertThat(planet.wrap(new Position(0, 9))).usingRecursiveComparison().isEqualTo(new Position(0, 7));
        assertThat(planet.wrap(new Position(0, 16))).usingRecursiveComparison().isEqualTo(new Position(0, 0));
        assertThat(planet.wrap(new Position(0, 19))).usingRecursiveComparison().isEqualTo(new Position(0, -3));
        assertThat(planet.wrap(new Position(0, 25))).usingRecursiveComparison().isEqualTo(new Position(0, -7));

        assertThat(planet.wrap(new Position(0, -7))).usingRecursiveComparison().isEqualTo(new Position(0, -7));
        assertThat(planet.wrap(new Position(0, -9))).usingRecursiveComparison().isEqualTo(new Position(0, -7));
        assertThat(planet.wrap(new Position(0, -16))).usingRecursiveComparison().isEqualTo(new Position(0, 0));
        assertThat(planet.wrap(new Position(0, -19))).usingRecursiveComparison().isEqualTo(new Position(0, 3));
        assertThat(planet.wrap(new Position(0, -25))).usingRecursiveComparison().isEqualTo(new Position(0, 7));
    }

    @Test
    void wrapShouldNotCrashWithNullPosition() {
        assertThat(planet.wrap(null)).isNull();
    }
}

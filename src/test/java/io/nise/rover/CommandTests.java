package io.nise.rover;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatNoException;

public class CommandTests {

    @Test
    void applyShouldNotCrashWithNullRover() {
        assertThatNoException().isThrownBy(() -> Command.F.apply(null));
    }
}

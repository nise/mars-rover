package io.nise.rover;

import java.util.Collections;
import java.util.stream.Collectors;

public class RoverCommandReader {
    CommandSequence readSequence(String sequence) {
        if (sequence == null) {
            return new CommandSequence(Collections.emptyList());
        }
        String unknownCommands = sequence.chars().filter(symbol -> !Command.fromSymbol((char) symbol).isPresent())
                .mapToObj(c -> String.valueOf((char) c)).collect(Collectors.joining());
        if (!unknownCommands.isEmpty()) {
            throw new IllegalArgumentException(String.format("Unknown rover commands: '%s'", unknownCommands));
        }

        return new CommandSequence(
                sequence.chars().mapToObj(c -> Command.fromSymbol((char) c).get()).collect(Collectors.toList()));
    }
}

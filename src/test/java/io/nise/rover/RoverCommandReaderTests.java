package io.nise.rover;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class RoverCommandReaderTests {

    RoverCommandReader reader = new RoverCommandReader();

    @Test
    void shouldProduceCommandSequence() {
        CommandSequence commandSequence = reader.readSequence("ffbbllrrflbr");
        assertThat(commandSequence.getCommands()).containsExactly(Command.F, Command.F, Command.B, Command.B, Command.L,
                Command.L, Command.R, Command.R, Command.F, Command.L, Command.B, Command.R);
    }

    @ParameterizedTest
    @ValueSource(strings = { "a", "fffa" })
    public void shouldThrowExceptionOnIllegalCommand(String sequence) {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> reader.readSequence(sequence))
                .withMessage("Unknown rover commands: 'a'");
    }

    @Test
    public void shouldNotCrashWithNullOrEmptySequence() {
        assertThat(reader.readSequence(null).getCommands()).isEmpty();
        assertThat(reader.readSequence("").getCommands()).isEmpty();
    }
}

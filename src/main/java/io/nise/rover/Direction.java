package io.nise.rover;

public enum Direction {
    N,
    E,
    S,
    W;

    public Direction rotateLeft() {
        return rotate(-1);
    }

    public Direction rotateRight() {
        return rotate(1);
    }

    private Direction rotate(int rotationValue) {
        Direction[] directions = values();
        int directionIndex = this.ordinal();
        return directions[(directions.length + (directionIndex + rotationValue)) % directions.length];
    }
}

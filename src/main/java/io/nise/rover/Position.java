package io.nise.rover;

public class Position {
    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Position translated(Vector vector) {
        if (vector == null) {
            return null;
        }
        if (vector.getDirection() == null) {
            throw new IllegalArgumentException("Must have direction for translation");
        }
        switch (vector.getDirection()) {
            case N:
                return new Position(this.x, this.y + vector.getIntensity());
            case E:
                return new Position(this.x - vector.getIntensity(), this.y);
            case S:
                return new Position(this.x, this.y - vector.getIntensity());
            case W:
                return new Position(this.x + vector.getIntensity(), this.y);
            default:
                throw new IllegalArgumentException("Must have known direction for translation");
        }
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", this.x, this.y);
    }
}

package io.nise.rover;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class RoverTests {

    Planet planet = new Planet(5, new Position(3, 2));
    Rover rover = new Rover(planet);
    RoverCommandReader reader = new RoverCommandReader();

    @Test
    public void shouldHBeAtOrigin() {
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(0, 0));
    }

    @Test
    public void shouldBeWestOriented() {
        assertThat(rover.getDirection()).isEqualTo(Direction.W);
    }

    @Test
    public void shouldIncreaseXPositionWhenMovingWest() {
        rover.executeCommand(reader.readSequence("f"));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(1, 0));
    }

    @Test
    public void shouldDecreaseXPositionWhenMovingEast() {
        rover.executeCommand(reader.readSequence("b"));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(-1, 0));
    }

    @ParameterizedTest
    @ValueSource(strings = { "r", "rrrrr" })
    public void shouldUpdateDirectionWhenTurningRight(String command) {
        rover.executeCommand(reader.readSequence(command));
        assertThat(rover.getDirection()).isEqualTo(Direction.N);
    }

    @ParameterizedTest
    @ValueSource(strings = { "l", "lllll" })
    public void shouldUpdateDirectionWhenTurningLeft(String command) {
        rover.executeCommand(reader.readSequence(command));
        assertThat(rover.getDirection()).isEqualTo(Direction.S);
    }

    @Test
    public void shouldExecuteMultipleCommands() {
        rover.executeCommand(reader.readSequence("fffb"));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(2, 0));
    }

    @Test
    public void shouldUpdatePositionAndDirectionWhenMovingAndTurning() {
        rover.executeCommand(reader.readSequence("rfffrbbbbr"));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(4, 3));
        assertThat(rover.getDirection()).isEqualTo(Direction.S);
    }

    @Test
    public void shouldNotFailForNullOrEmptyCommand() {
        rover.executeCommand(null);
        rover.executeCommand(reader.readSequence(""));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(0, 0));
        assertThat(rover.getDirection()).isEqualTo(Direction.W);
    }

    @Test
    public void shouldWrapAroundThePlanetAndNotChangeDirection() {
        rover.executeCommand(reader.readSequence("ffffff"));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(1, 0));
        assertThat(rover.getDirection()).isEqualTo(Direction.W);
    }

    @Test
    public void shouldWrapOnPoleAndTurnAround() {
        rover.executeCommand(reader.readSequence("rffffff"));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(0, 4));
        assertThat(rover.getDirection()).isEqualTo(Direction.S);
    }

    @Test
    public void shouldWrapOnPoleAndTurnAroundWhenGoingBackToo() {
        rover.executeCommand(reader.readSequence("rbbbbbb"));
        assertThat(rover.getPosition()).usingRecursiveComparison().isEqualTo(new Position(0, -4));
        assertThat(rover.getDirection()).isEqualTo(Direction.S);
    }

    @Test
    public void shouldStopBeforeObstacleAndReportPosition() {
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(
                () -> rover.executeCommand(reader.readSequence("fffrfff")))
                .withMessage("Obstacle detected, cannot move to position (3, 2). Staying at position (3, 1).");
    }

    @Test
    public void detectObstacleShouldNotCrashWithNullPosition() {
        assertThat(rover.detectObstacle(null)).isFalse();
    }
}

package io.nise.rover;

import java.util.List;

public class CommandSequence {
    private final List<Command> commands;

    public CommandSequence(java.util.List<Command> commands) {
        this.commands = commands;
    }

    public List<Command> getCommands() {
        return commands;
    }
}

package io.nise.rover;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Command {
    F('f', rover -> {
        rover.moveForward();
    }),
    B('b', rover -> {
        rover.moveBackward();
    }),
    R('r', rover -> {
        rover.turnRight();
    }),
    L('l', rover -> {
        rover.turnLeft();
    });

    private static final Map<Character, Command> symbolToCommand = Stream.of(values())
            .collect(Collectors.toMap(command -> command.symbol, command -> command));

    private final char symbol;
    private final Consumer<Rover> roverAction;

    Command(char symbol, Consumer<Rover> roverAction) {
        this.symbol = symbol;
        this.roverAction = roverAction;
    }

    public static Optional<Command> fromSymbol(char symbol) {
        return Optional.ofNullable(symbolToCommand.get(symbol));
    }

    public void apply(Rover rover) {
        if (rover != null) {
            this.roverAction.accept(rover);
        }
    }

    public char getSymbol() {
        return this.symbol;
    }
}

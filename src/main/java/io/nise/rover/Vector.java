package io.nise.rover;

public class Vector {
    private final Direction direction;
    private final int intensity;

    public Vector(Direction direction, int intensity) {
        this.direction = direction;
        this.intensity = intensity;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getIntensity() {
        return intensity;
    }
}

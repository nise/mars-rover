package io.nise.rover;

public class Rover {
    private Position position = new Position(0, 0);
    private Direction direction = Direction.W;
    private Planet planet;

    public Rover(Planet planet) {
        this.planet = planet;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void executeCommand(CommandSequence sequence) {
        if (sequence != null) {
            for (Command command : sequence.getCommands()) {
                command.apply(this);
                int unwrappedY = this.position.getY();
                this.position = planet.wrap(this.position);
                // If the rover has been wrapped vertically, it now faces the opposite
                // direction.
                if (this.position.getY() != unwrappedY) {
                    Command.L.apply(this);
                    Command.L.apply(this);
                }
            }
        }
    }

    public boolean detectObstacle(Position position) {
        return planet.hasObstacle(position);
    }

    private void translateRover(int intensity) {
        Position newPosition = this.getPosition().translated(new Vector(this.getDirection(), intensity));
        if (this.detectObstacle(newPosition)) {
            throw new IllegalStateException(String.format(
                    "Obstacle detected, cannot move to position %s. Staying at position %s.",
                    newPosition, this.getPosition()));
        }
        this.setPosition(newPosition);
    }

    public void moveForward() {
        this.translateRover(1);
    }

    public void moveBackward() {
        this.translateRover(-1);
    }

    public void turnRight() {
        this.setDirection(this.direction.rotateRight());
    }

    public void turnLeft() {
        this.setDirection(this.direction.rotateLeft());
    }
}

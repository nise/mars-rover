# Mars rover kata

This repository contains my implementation of the mars rover kata which can be found [here](https://kata-log.rocks/mars-rover-kata).

It does not include a `Main` class. The unit tests can be run with maven by cd'ing into the project and executing the command `mvn test`.

The rover can be sent a sequence of commands through its `executeCommand` method by providing a `CommandSequence` instance as argument. It can be generated from a string whose valid characters are `f` (forward), `b` (back), `l` (turn left) or `r` (turn right) using a `RoverCommandReader` instance. If any other characters are present in the string the reader throws an `IllegalArgumentException` and reports the illegal characters.

The planet has been reified in the `Planet` class, allowing to set its size and place obstacles using its constructor, and wrap positions around it using its `wrap` method. The wrapping algorithm works as follows:

- The rover continually turns around the planet on the x axis, so that if the x position reaches `size + 1`, it is converted to 0 and the rover starts over from the origin.
- x position is always positive. It could be seen as the longitude. y position can be negative, with its origin on the equator. It could be seen as the latitude.
- If the rover reaches a pole (maximal or minimal value in y position), it turns around and keeps going in the opposite direction in the y axis. So for a planet of a size of 2 for example, if the rover turns north and then keeps going forward, its sequence of y positions would be [0, 1, 2, 1, 0, -1, -2, -1, 0, … ]. In this case for simplicity's sake it has been arbitrarily decided that the x position stays unchanged, although one could argue that if the rover reaches a pole, it would then be on the diametrical opposite of the planet longitude-wise.

If the rover detects an obstacle, it stops before going to the next position and throws an `IllegalStateException`. As a further development of this project, obstacle bypassing could be implemented. A command line interface for sending commands to the rover could also easily be implemented.
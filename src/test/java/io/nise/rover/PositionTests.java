package io.nise.rover;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class PositionTests {

    Position position = new Position(0, 0);

    @Test
    void shouldTranslateDependingOnDirection() {
        assertThat(position.translated(new Vector(Direction.N, 1))).usingRecursiveComparison()
                .isEqualTo(new Position(0, 1));
        assertThat(position.translated(new Vector(Direction.S, 1))).usingRecursiveComparison()
                .isEqualTo(new Position(0, -1));
        assertThat(position.translated(new Vector(Direction.E, 1))).usingRecursiveComparison()
                .isEqualTo(new Position(-1, 0));
        assertThat(position.translated(new Vector(Direction.W, 1))).usingRecursiveComparison()
                .isEqualTo(new Position(1, 0));
    }

    @Test
    void shouldFailWithNullDirectionTranslation() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> position.translated(new Vector(null, 0)))
                .withMessage("Must have direction for translation");
    }

    @Test
    void shouldConvertToString() {
        assertThat(String.format("%s", new Position(1, 2))).isEqualTo("(1, 2)");
    }

    @Test
    void shouldNotFailWithNullVector() {
        assertThat(position.translated(null)).isNull();
    }
}

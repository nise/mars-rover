package io.nise.rover;

import java.util.Arrays;

public class Planet {

    private int radius;
    private final Position[] obstacles;

    public Planet(int radius, Position... obstacles) {
        this.radius = radius;
        this.obstacles = obstacles;
    }

    public int getRadius() {
        return radius;
    }

    public Position[] getObstacles() {
        return obstacles;
    }

    public Position wrap(Position position) {
        if (position == null) {
            return null;
        }
        int absY = Math.abs(position.getY());
        // number of time we hit the poles or the equator
        int quotient = Math.floorDiv(absY, this.radius);
        int y = 0;
        // the position goes up, then down on the North Pole, then down from the
        // equator, then up again from the South Pole.
        switch (quotient % 4) {
            case 0:
                y = absY % this.radius;
                break;
            case 1:
                y = this.radius - absY % this.radius;
                break;
            case 2:
                y = -absY % this.radius;
                break;
            case 3:
                y = absY % this.radius - this.radius;
                break;
        }
        return new Position(position.getX() % this.radius,
                // If the original y position is negative, it's the other way around, so it must
                // be negated.
                (int) Math.copySign(1, position.getY()) * y);
    }

    public boolean hasObstacle(Position position) {
        if (position == null) {
            return false;
        }
        Position wrappedPosition = this.wrap(position);
        return Arrays.stream(this.getObstacles())
                .anyMatch(o -> o.getX() == wrappedPosition.getX() && o.getY() == wrappedPosition.getY());
    }
}
